var playState = {
    create: function() {

      console.log(game.global.state);

      game.physics.startSystem(Phaser.Physics.ARCADE);
      
      bgtile = game.add.tileSprite(0, 0, game.width, game.cache.getImage('bgtile').height, 'bgtile');
      this.bgm = game.add.audio('bgm');
      this.bgm.loopFull();

      // PAUSE
      pause_label = game.add.text(20, game.height - 40, 'Pause', { font: '24px Arial', fill: '#FF8080'});
      pause_label.inputEnabled = true;
      pause_label.events.onInputUp.add(function(){
          game.paused = true;
          remind_label = game.add.text(game.width/2, game.height*3/4, 'Click any place to continue', {font: '30px Arial', fill: '#FF8080'});
          remind_label.anchor.setTo(0.5, 0.5);
      });
      game.input.onDown.add(unpause, self);
      function unpause(event){
          if(game.paused){
              remind_label.destroy();
              game.paused = false;
          }
      }
      
      // PLAYER
      this.player = game.add.sprite(game.width/4, game.height/2, 'player');
      this.player.bloodvalue = 100;
      game.physics.arcade.enable(this.player);
      this.player.three = 0;
      // PLAYER2
      if(game.global.state == 2){
        this.player2 = game.add.sprite(game.width/4, game.height/2, 'player2');
        this.player2.bloodvalue = 100;
        game.physics.arcade.enable(this.player2);
        this.player2.three = 0;
      }
  
      // INPUT
      this.cursor = game.input.keyboard.createCursorKeys();
      this.key = game.input.keyboard;
  
      // HEART
      this.heart = game.add.sprite(60, 140, 'heart');
      game.physics.arcade.enable(this.heart);
      this.heart.anchor.setTo(0.5, 0.5);
      this.heart.scale.setTo(0.3, 0.3);
      this.nextheartat = 5000;

      // HEART
      this.three = game.add.sprite(60, 300, 'three');
      game.physics.arcade.enable(this.three);
      this.three.anchor.setTo(0.5, 0.5);
      this.three.scale.setTo(0.3, 0.3);
      this.nextthreeat = 6000;
  
      // SCORE
      game.global.score = 0;
      this.scoreLabel = game.add.text(20, 15, 'score: 0', { font: '18px Arial', fill: '#ffffff' });
      // BLOOD
      //game.global.blood = 100;
      this.bloodLabel = game.add.text(game.width-100, 15, 'blood: 100', { font: '18px Arial', fill: '#ffffff' });
      if(game.global.state == 2){
        this.bloodLabel2 = game.add.text(game.width-100, game.height - 55, 'blood: 100', { font: '18px Arial', fill: '#FF8080' });
      }
      // SKILL
      this.player.skill = 0;
      this.skillLabel = game.add.text(game.width-100, 40, 'skills: 0', { font: '18px Arial', fill: '#ffffff' });
      if(game.global.state == 2){
        this.player2.skill = 0;
        this.skillLabel2 = game.add.text(game.width-100, game.height - 30, 'skills: 0', { font: '18px Arial', fill: '#FF8080' });
      }
      // LEVEL
      game.global.level = 0;
      this.levelLabel = game.add.text(20, 40, 'level: 1', { font: '18px Arial', fill: '#ffffff' });


      //BIRD
      this.birds = game.add.group();
      this.birds.enableBody = true;
      this.birds.createMultiple(50,'bird');
      this.birds.setAll('anchor.x',0.5);
      this.birds.setAll('anchor.y',0.5);
      this.birds.setAll('outOfBoundsKill',true);
      this.birds.setAll('checkWorldBounds',true);
      this.birds.forEach(function(bird){
        bird.animations.add('fly',[0,1,2,3,4,5,6,7],8,true);
        bird.nextseedat = 0;
      });
      this.nextBirdAt = 1000;
      this.birdDelay = 2000;
      //BIRD_Y
      this.birds_y = game.add.group();
      this.birds_y.enableBody = true;
      this.birds_y.createMultiple(20,'bird_y');
      this.birds_y.setAll('anchor.x',0.5);
      this.birds_y.setAll('anchor.y',0.5);
      this.birds_y.setAll('outofBoundsKill',true);
      this.birds_y.setAll('checkWorldBounds',true);
      this.birds_y.forEach(function(bird_y){
        bird_y.animations.add('fly',[0,1,2,3,4,5,6,7],8,true);
        bird_y.nextseedat = 0;
      })
      this.nextBirdYAt = 10000;
      this.birdYDelay = 2500;
      this.changeDelay = 1000;
  
      //BIRD_R
      this.birds_r = game.add.group();
      this.birds_r.enableBody = true;
      this.birds_r.createMultiple(20,'bird_r');
      this.birds_r.setAll('anchor.x',0.5);
      this.birds_r.setAll('anchor.y',0.5);
      this.birds_r.setAll('outofBoundsKill',true);
      this.birds_r.setAll('checkWorldBounds',true);
      this.birds_r.forEach(function(bird_r){
          bird_r.animations.add('fly',[0,1,2,3,4,5,6,7],8,true);
          bird_r.nextseedat = 0;
      })
      this.nextBirdRAt = 20000;
      this.birdRDelay = 3000;

      //BOSS
      this.bosses = game.add.group();
      this.bosses.enableBody = true;
      this.bosses.createMultiple(2, 'boss');
      this.bosses.setAll('anchor.x',0.5);
      this.bosses.setAll('anchor.y',0.5);
      this.bosses.setAll('outofBoundsKill',true);
      this.bosses.setAll('checkWorldBounds',true);
      this.bosses.forEach(function(boss){
        boss.animations.add('fly',[0,1,2,3,4,5,6,7],8,true);
        boss.nextseedat = 0;
      })
      this.boss_num = 0;
      this.boss_blood = 500;

      // BULLET
      this.bullets = game.add.group();
      this.bullets.enableBody = true;
      this.bullets.createMultiple(45, 'bullet');
      this.bullets.setAll('anchor.x',0.5);
      this.bullets.setAll('anchor.y',0.5);
      this.bullets.setAll('outOfBoundsKill',true);
      this.bullets.setAll('checkWorldBounds',true);
      this.player.nextShotAt = 0;
      if(game.global.state == 2)this.player2.nextShotAt = 0;
      this.shotDelay = 200;
      this.nextShot2At = 0;
      if(game.global.state == 2)this.player2.nextShot2At = 0;
      this.shot2Delay = 400;
  
      // SEED
      this.seeds = game.add.group();
      this.seeds.enableBody = true;
      this.seeds.createMultiple(700, 'bullet');
      this.seeds.setAll('anchor.x',0.5);
      this.seeds.setAll('anchor.y',0.5);
      this.seeds.setAll('outOfBoundsKill',true);
      this.seeds.setAll('checkWorldBounds',true);
      this.seedys = game.add.group();
      this.seedys.enableBody = true;
      this.seedys.createMultiple(700, 'seed_r');
      this.seedys.setAll('anchor.x',0.5);
      this.seedys.setAll('anchor.y',0.5);
      this.seedys.setAll('outOfBoundsKill',true);
      this.seedys.setAll('checkWorldBounds',true);
      this.seedrs = game.add.group();
      this.seedrs.enableBody = true;
      this.seedrs.createMultiple(700, 'seed_y');
      this.seedrs.setAll('anchor.x',0.5);
      this.seedrs.setAll('anchor.y',0.5);
      this.seedrs.setAll('outOfBoundsKill',true);
      this.seedrs.setAll('checkWorldBounds',true);
      this.seedDelay = 1500;
      this.seedRDelay = 1500;
      this.seedYDelay = 1500;
  
      // PARTICLE
      this.emitter = game.add.emitter(0, 0, 30);
      this.emitter.makeParticles('pixel');
      this.emitter.setYSpeed(-150, 150);
      this.emitter.setXSpeed(-150, 150);
      this.emitter.setScale(2, 0, 2, 0, 600);
      this.emitter.gravity = 0;

      game.global.timerecord = game.time.now;
      this.flag = 0;
    },
    update: function() {
      if(this.flag == 0){
        console.log(game.global.timerecord);
        this.flag = 1;
      }
      game.global.time = (game.time.now-game.global.timerecord)/1000;
      //this.timeLabel.text = 'time: ' + game.global.time;
      bgtile.tilePosition.x -= 1;
      game.physics.arcade.overlap(this.player, this.birds, this.playerHit2, null, this);
      game.physics.arcade.overlap(this.player, this.birds_y, this.playerHit2, null, this);
      game.physics.arcade.overlap(this.player, this.birds_r, this.playerHit2, null, this);
      game.physics.arcade.overlap(this.player, this.heart, this.takeheart, null, this);
      game.physics.arcade.overlap(this.player, this.three, this.takethree, null, this);

      if(game.global.state == 2){
        game.physics.arcade.overlap(this.player2, this.birds, this.playerHit2, null, this);
        game.physics.arcade.overlap(this.player2, this.birds_y, this.playerHit2, null, this);
        game.physics.arcade.overlap(this.player2, this.birds_r, this.playerHit2, null, this);
        game.physics.arcade.overlap(this.player2, this.heart, this.takeheart, null, this);
        game.physics.arcade.overlap(this.player2, this.three, this.takethree, null,this);
        game.physics.arcade.overlap(this.player2, this.seeds, this.playerHit, null, this);
        game.physics.arcade.overlap(this.player2, this.seedrs, this.playerHit, null, this);
        game.physics.arcade.overlap(this.player2, this.seedys, this.playerHit, null, this);
      }

      game.physics.arcade.overlap(this.bullets, this.birds, this.birdHit, null, this);
      game.physics.arcade.overlap(this.bullets, this.birds_y, this.birdHit_y, null, this);
      game.physics.arcade.overlap(this.bullets, this.birds_r, this.birdHit_r, null, this);
      game.physics.arcade.overlap(this.bullets, this.bosses, this.bossHit, null, this);
      game.physics.arcade.overlap(this.player, this.seeds, this.playerHit, null, this);
      //game.physics.arcade.overlap(this.bullets, this.seeds, this.seedHit, null, this);
      game.physics.arcade.overlap(this.player, this.seedrs, this.playerHit, null, this);
      //game.physics.arcade.overlap(this.bullets, this.seedrs, this.seedHit, null, this);
      game.physics.arcade.overlap(this.player, this.seedys, this.playerHit, null, this);
      //game.physics.arcade.overlap(this.bullets, this.seedys, this.seedHit, null, this);

      if(game.global.score >= 50 && game.global.score <100){
        game.global.level = 2;
        this.levelLabel.text = 'level: ' + game.global.level;
      }else if(game.global.score >= 100 && game.global.score < 200){
        game.global.level = 3;
        this.levelLabel.text = 'level: ' + game.global.level;
      }else if(game.global.score >= 200){
        game.global.level = 4;
        this.levelLabel.text = 'level: ' + game.global.level;
      }else{
        game.global.level = 1;
        this.levelLabel.text = 'level: ' + game.global.level;
      }
  
      if (this.nextBirdAt<(game.time.now-game.global.timerecord)){
        game.global.score += 2;
        this.scoreLabel.text = 'score: ' + game.global.score;
        this.player.skill += 2;
        this.skillLabel.text = 'skills: ' + this.player.skill;
        if(game.global.state == 2){
          this.player2.skill += 2;
          this.skillLabel2.text = 'skills: ' + this.player2.skill;
        }
        this.nextBirdAt = (game.time.now-game.global.timerecord) + this.birdDelay;
        var bird = this.birds.getFirstExists(false);
        bird.reset(game.width, game.rnd.integerInRange(0,game.height),'bird');
        bird.body.velocity.x = -1*game.rnd.integerInRange(80,160);
        bird.body.velocity.y = 0;
        bird.animations.play('fly');
      }
      if(this.nextBirdYAt<(game.time.now-game.global.timerecord) && game.global.score >= 50){
          this.nextBirdYAt = (game.time.now-game.global.timerecord) + this.birdYDelay;
          var bird_y = this.birds_y.getFirstExists(false);
          bird_y.reset(game.width, game.rnd.integerInRange(0,game.height),'bird_y');
          bird_y.body.velocity.x = -1*game.rnd.integerInRange(80,160);
          bird_y.body.velocity.y = 0;
          bird_y.animations.play('fly');
      }
      if(this.nextheartat<(game.time.now-game.global.timerecord) && !this.heart.alive){
        this.heart.reset(game.rnd.integerInRange(20,game.width-100), game.rnd.integerInRange(20,game.height-20));
      }
      if(this.nextthreeat<(game.time.now-game.global.timerecord) && !this.three.alive){
        this.three.reset(game.rnd.integerInRange(20,game.width-100), game.rnd.integerInRange(20,game.height-20));
      }
      if(this.nextBirdRAt<(game.time.now-game.global.timerecord) && game.global.score >= 100){
          this.nextBirdRAt = (game.time.now-game.global.timerecord) + this.birdRDelay;
          var bird_r = this.birds_r.getFirstExists(false);
          bird_r.reset(game.width, game.rnd.integerInRange(0,game.height),'bird_r');
          bird_r.body.velocity.x = -1*game.rnd.integerInRange(80,160);
          bird_r.body.velocity.y = 0;
          bird_r.animations.play('fly');
      }
      if(this.boss_num == 0 && game.global.score >=200){
        this.boss_num = 1;
        var boss = this.bosses.getFirstExists(false);
        boss.reset(game.width, game.height/2,'boss');
        boss.scale.setTo(2,2);
        boss.body.velocity.x = 0;
        boss.body.velocity.y = 0;
        boss.animations.play('fly');
      }
  
      this.shootSeed();
      this.changeSpeed();
  
      this.movePlayer();
      if ((!this.player.inWorld || this.player.bloodvalue <= 0) && this.player.alive) { this.playerDie();}
      if(game.global.state == 2)if ((!this.player2.inWorld || this.player2.bloodvalue <= 0) && this.player2.alive) { this.player2Die();}
    }, // No changes
    movePlayer: function() {
  
      // LEFT RIGHT
      if (this.cursor.left.isDown) {
        this.player.body.velocity.x = -200;
      }
      else if (this.cursor.right.isDown) {
        this.player.body.velocity.x = 200;
      }
      else {
        this.player.body.velocity.x = 0;
        this.player.animations.stop();
      }  
      // UP DOWN
      if (this.cursor.up.isDown) {
        this.player.body.velocity.y = -200;
        this.player.animations.stop();
        this.player.frame = 0; // Set frame (0 : stand)
      }
      else if (this.cursor.down.isDown) {
        this.player.body.velocity.y = 200;
        this.player.animations.stop();
        this.player.frame = 2; // Set frame (0 : stand)
      }
      else {
        this.player.body.velocity.y = 0;
        this.player.animations.stop();
        this.player.frame = 1; // Set frame (0 : stand)
      }
      // BULLET
      if (game.input.keyboard.isDown(Phaser.Keyboard.O)) {
        this.fire(this.player);
      } else if(game.input.keyboard.isDown(Phaser.Keyboard.P)) {
        this.fire2(this.player);
      }
  
      if (game.input.keyboard.isDown(Phaser.Keyboard.I)) {
          this.sound.volume -= 0.01;
          this.bgm.volume -= 0.01;
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.U)) {
          this.sound.volume += 0.01;
          this.bgm.volume += 0.01;
      }

      if(game.global.state == 1)return;

      // LEFT RIGHT
      if (game.input.keyboard.isDown(Phaser.Keyboard.A)) {
        this.player2.body.velocity.x = -200;
      }
      else if (game.input.keyboard.isDown(Phaser.Keyboard.D)) {
        this.player2.body.velocity.x = 200;
      }
      else {
        this.player2.body.velocity.x = 0;
        this.player2.animations.stop();
      }  
      // UP DOWN
      if (game.input.keyboard.isDown(Phaser.Keyboard.W)) {
        this.player2.body.velocity.y = -200;
        this.player2.animations.stop();
        this.player2.frame = 0; // Set frame (0 : stand)
      }
      else if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
        this.player2.body.velocity.y = 200;
        this.player2.animations.stop();
        this.player2.frame = 2; // Set frame (0 : stand)
      }
      else {
        this.player2.body.velocity.y = 0;
        this.player2.animations.stop();
        this.player2.frame = 1; // Set frame (0 : stand)
      }
      // BULLET
      if (game.input.keyboard.isDown(Phaser.Keyboard.G)) {
        this.fire(this.player2);
      } else if(game.input.keyboard.isDown(Phaser.Keyboard.H)) {
        this.fire2(this.player2);
      }

    },
    seedHit: function(bullet, seed){
      bullet.kill();
      seed.kill();
    },
    birdHit: function(bullet, bird){
      game.global.score += 5;
      this.scoreLabel.text = 'score: ' + game.global.score;
      this.player.skill += 5;
      this.skillLabel.text = 'skills: ' + this.player.skill;
      if(game.global.state == 2){
        this.player2.skill += 5;
        this.skillLabel2.text = 'skills: ' + this.player2.skill;
      }
      
      this.emitter.x = bullet.x;
      this.emitter.y = bullet.y;
      this.emitter.start(true, 600, null, 15);
      
      bullet.kill();
      bird.kill();
    },
    birdHit_y: function(bullet, bird){
      game.global.score += 10;
      this.scoreLabel.text = 'score: ' + game.global.score;
      this.player.skill += 10;
      this.skillLabel.text = 'skills: ' + this.player.skill;
      if(game.global.state == 2){
        this.player2.skill += 10;
        this.skillLabel2.text = 'skills: ' + this.player2.skill;
      }
      this.emitter.x = bullet.x;
      this.emitter.y = bullet.y;
      this.emitter.start(true, 600, null, 15);
      bullet.kill();
      bird.kill();
    },
    birdHit_r: function(bullet, bird){
      game.global.score += 15;
      this.scoreLabel.text = 'score: ' + game.global.score;
      this.player.skill += 15;
      this.skillLabel.text = 'skills: ' + this.player.skill;
      if(game.global.state == 2){
        this.player2.skill += 15;
        this.skillLabel2.text = 'skills: ' + this.player2.skill;
      }
      this.emitter.x = bullet.x;
      this.emitter.y = bullet.y;
      this.emitter.start(true, 600, null, 15);
      bullet.kill();
      bird.kill();
    },
    bossHit: function(bullet, bird){
      game.global.scale += 5;
      this.scoreLabel.text = 'score: ' + game.global.score;
      this.player.skill += 5;
      this.skillLabel.text = 'skills: ' + this.player.skill;
      if(game.global.state == 2){
        this.player2.skill += 5;
        this.skillLabel2.text = 'skills: ' + this.player2.skill;
      }
      this.emitter.x = bullet.x;
      this.emitter.y = bullet.y;
      this.emitter.start(true, 600, null, 15);
      bullet.kill();
      this.boss_blood -= 10;
      if(this.boss_blood<=0){
        this.bossDie(bird);
      }
    },
    fire: function(player){
      if(player.nextShotAt>(game.time.now-game.global.timerecord)){
        return;
      }
      if(this.bullets.countDead()==0){
        return;
      }
      player.nextShotAt = (game.time.now-game.global.timerecord) + this.shotDelay;

      if(player.three == 0){
        var bullet = this.bullets.getFirstExists(false);
        bullet.reset(player.x+50, player.y+20);
        bullet.scale.setTo(0.5,0.5);
        bullet.body.velocity.x = 500;
      }else {
        var bullet = this.bullets.getFirstExists(false);
        bullet.reset(player.x+50, player.y+20);
        bullet.scale.setTo(0.5,0.5);
        bullet.body.velocity.x = 500;
        bullet = this.bullets.getFirstExists(false);
        bullet.reset(player.x+50, player.y+30);
        bullet.scale.setTo(0.5,0.5);
        bullet.body.velocity.x = 500;
        bullet = this.bullets.getFirstExists(false);
        bullet.reset(player.x+50, player.y+10);
        bullet.scale.setTo(0.5,0.5);
        bullet.body.velocity.x = 500;
        player.three -= 1;
      }
      this.sound.play('shoot');
    },
    fire2: function(player){
      if(player.nextShot2At>(game.time.now-game.global.timerecord)){
        return;
      }
      if(this.bullets.countDead()<=20 || player.skill < 30){
        return;
      }
      player.skill -= 30;
      this.skillLabel.text = 'skills: ' + player.skill;

      player.nextShot2At = (game.time.now-game.global.timerecord) + this.shotDelay;

      var bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      bullet.body.velocity.x = 500;

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(30, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(60, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(90, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(120, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(150, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(180, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(-30, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(-60, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(-90, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(-120, 500, bullet.body.velocity);

      bullet = this.bullets.getFirstExists(false);
      bullet.reset(player.x+50, player.y+20);
      bullet.scale.setTo(0.5,0.5);
      game.physics.arcade.velocityFromAngle(-150, 500, bullet.body.velocity);

      this.sound.play('shoot');

    },
    changeSpeed: function(){
        var changeDelay = this.changeDelay;
        this.birds_y.forEach(function(bird_y){
            if(!bird_y.alive)return;
            if(!bird_y.nextchangeat)bird_y.nextchangeat = 0;
            if(bird_y.y>game.height-40){
                bird_y.nextchangeat = (game.time.now-game.global.timerecord) + changeDelay;
                bird_y.body.velocity.y = -100;
                return;
            }else if(bird_y.y<40){
                bird_y.nextchangeat = (game.time.now-game.global.timerecord) + changeDelay;
                bird_y.body.velocity.y = 100;
                return;
            }else if(bird_y.nextchangeat>(game.time.now-game.global.timerecord))return;
            bird_y.nextchangeat = (game.time.now-game.global.timerecord) + changeDelay;
            bird_y.body.velocity.y = 100 * game.rnd.pick([1,-1]);
        })
        this.bosses.forEach(function(boss){
          if(!boss.alive)return;
          if(!boss.nextchangeat){
            boss.nextchangeat = 0;
            boss.body.velocity.y = 100;
            boss.body.velocity.x = 50;
          }
          if(boss.nextchangeat>(game.time.now-game.global.timerecord))return;
          else{
            boss.nextchangeat = (game.time.now-game.global.timerecord) + 1000;
            if(boss.x <= game.width/2)boss.body.velocity.x = 50;
            else if(boss.x >= game.width - 50)boss.body.velocity.x = -50;
            else boss.body.velocity.x = boss.body.velocity.x * game.rnd.pick([1,-1]);
            if(boss.y <= 80) boss.body.velocity.y = 100;
            else if(boss.y >= game.height - 80)boss.body.velocity.y = -100;
            else boss.body.velocity.y = boss.body.velocity.y * game.rnd.pick([1,-1]);
          }
        })
    },
    shootSeed: function(){
      var seeds = this.seeds;
      var seedDelay = this.seedDelay;
      var seedRDelay = this.seedRDelay;
      var seedYDelay = this.seedYDelay;
      var player = this.player;
      var seedys = this.seedys;
      var seedrs = this.seedrs;
      this.birds.forEach(function(bird){
        if(!bird.alive){
          return;
        }
        if(!bird.nextseedat){
          bird.nextseedat = 0;
        }
        if(bird.nextseedat>(game.time.now-game.global.timerecord)){
          return;
        }
        if(seeds.countDead()==0){
          return;
        }
        bird.nextseedat = (game.time.now-game.global.timerecord) + seedDelay;
        var seed = seeds.getFirstExists(false);
        seed.reset(bird.x,bird.y);
        seed.scale.setTo(0.2,0.2);
        seed.body.velocity.x = -200;
      });
      this.birds_y.forEach(function(bird_y){
        if(!bird_y.alive){
          return;
        }
        if(!bird_y.nextseedat){
          bird_y.nextseedat = 0;
        }
        if(bird_y.nextseedat>(game.time.now-game.global.timerecord)){
          return;
        }
        if(seedys.countDead()==0){
          return;
        }
        bird_y.nextseedat = (game.time.now-game.global.timerecord) + seedYDelay;
        var seedy = seedys.getFirstExists(false);
        seedy.reset(bird_y.x,bird_y.y);
        seedy.scale.setTo(0.2,0.2);
        seedy.body.velocity.x = -200;
      });
      this.birds_r.forEach(function(bird_r){
        if(!bird_r.alive){
          return;
        }
        if(!bird_r.nextseedat){
          bird_r.nextseedat = 0;
        }
        if(bird_r.nextseedat>(game.time.now-game.global.timerecord)){
          return;
        }
        if(seedrs.countDead()==0){
          return;
        }
        bird_r.nextseedat = (game.time.now-game.global.timerecord) + seedRDelay;
        var seedr = seedrs.getFirstExists(false);
        seedr.reset(bird_r.x,bird_r.y);
        seedr.scale.setTo(0.2,0.2);
        var angleDeg = (Math.atan2(player.y - bird_r.y, player.x - bird_r.x) * 180 / Math.PI);
        game.physics.arcade.velocityFromAngle(angleDeg, 200, seedr.body.velocity);
      });
      this.bosses.forEach(function(boss){
        if(!boss.alive){
          return;
        }
        if(!boss.nextseedat){
          boss.nextseedat = 0;
        }
        if(boss.nextseedat>(game.time.now-game.global.timerecord)){
          return;
        }
        if(seedrs.countDead()==0){
          return;
        }
        boss.nextseedat = (game.time.now-game.global.timerecord) + 1000;
        var seedr = seedrs.getFirstExists(false);

        seedr.reset(boss.x-50, boss.y);
        seedr.scale.setTo(0.3,0.3);
        seedr.body.velocity.x = -500;

        seedr = seedrs.getFirstExists(false);
        seedr.reset(boss.x-50, boss.y);
        seedr.scale.setTo(0.3,0.3);
        game.physics.arcade.velocityFromAngle(150, 500, seedr.body.velocity);
        
        seedr = seedrs.getFirstExists(false);
        seedr.reset(boss.x-50, boss.y);
        seedr.scale.setTo(0.3,0.3);
        game.physics.arcade.velocityFromAngle(210, 500, seedr.body.velocity);

      });
    },
    takeheart: function(player, heart) {
      if(player.bloodvalue < 100)player.bloodvalue += 5;
      this.bloodLabel.text = 'blood: ' + this.player.bloodvalue;
      if(game.global.state == 2)this.bloodLabel2.text = 'blood: ' + this.player2.bloodvalue;
      
      // NEW POSITION
      // var heartPosition = [{x: 140, y: 60}, {x: 360, y: 60},
      //                     {x: 60, y: 140}, {x: 440, y: 140},
      //                     {x: 130, y: 300}, {x: 370, y: 300}];
      // for (var i = 0; i < heartPosition.length; i++) {
      //   if (heartPosition[i].x == this.heart.x) {
      //     heartPosition.splice(i, 1);
      //   }
      // }
      // var newPosition = game.rnd.pick(heartPosition);
      heart.kill();
      this.nextheartat = (game.time.now-game.global.timerecord) + game.rnd.integerInRange(10000, 20000);
  
      // TWEEN
      this.heart.scale.setTo(0, 0);
      game.add.tween(this.heart.scale).to({x: 0.3, y: 0.3}, 300).start();
      game.add.tween(player.scale).to({x: 1.5, y: 1.5}, 100).yoyo(true).start();
    },
    takethree: function(player, three) {
      
      three.kill();
      this.nextthreeat = (game.time.now-game.global.timerecord) + game.rnd.integerInRange(10000, 20000);
      player.three = 10;
  
      // TWEEN
      this.three.scale.setTo(0, 0);
      game.add.tween(this.three.scale).to({x: 0.3, y: 0.3}, 300).start();
      game.add.tween(player.scale).to({x: 1.5, y: 1.5}, 100).yoyo(true).start();
    },
    playerHit: function(player,seed) {

      this.emitter.x = seed.x;
      this.emitter.y = seed.y;
      this.emitter.start(true, 600, null, 15);

      seed.kill();
      
      player.bloodvalue -= 5;
      this.bloodLabel.text = 'blood: ' + this.player.bloodvalue;
      if(game.global.state == 2)this.bloodLabel2.text = 'blood: ' + this.player2.bloodvalue;
      game.camera.shake(0.01, 100);
    },
    playerHit2: function(player,bird) {
        bird.kill();

        
        this.emitter.x = player.x;
        this.emitter.y = player.y;
        this.emitter.start(true, 600, null, 15);

        player.bloodvalue -= 10;
        this.bloodLabel.text = 'blood: ' + this.player.bloodvalue;
        if(game.global.state == 2)this.bloodLabel2.text = 'blood: ' + this.player2.bloodvalue;
        game.camera.shake(0.05, 300);
    },
    playerDie: function() {
      this.player.bloodvalue = 0;
      this.player.kill();
  
      // PARTICLE SYSTEM
      this.emitter.x = this.player.x;
      this.emitter.y = this.player.y;
      this.emitter.start(true, 800, null, 15);
  
      // CAMERA EFFECT
      //game.camera.flash(0xffffff, 300);
      game.camera.shake(0.02, 300);

      if(game.global.state != 2 || (game.global.state == 2 && !this.player2.alive)){
        game.time.events.add(1000, function() {
          game.state.start('menu');
        }, this);
      }
    },
    player2Die: function() {
      this.player2.kill();
  
      // PARTICLE SYSTEM
      this.emitter.x = this.player2.x;
      this.emitter.y = this.player2.y;
      this.emitter.start(true, 800, null, 15);
  
      // CAMERA EFFECT
      //game.camera.flash(0xffffff, 300);
      game.camera.shake(0.02, 300);
      if(!this.player.alive){
        game.time.events.add(1000, function() {
          game.state.start('menu');
        }, this);
      }
    },
    bossDie: function(bird) {
      game.global.score += 100;
      this.scoreLabel.text = 'score: ' + game.global.score;
      
      bird.kill();
      
      // PARTICLE SYSTEM
      this.emitter.x = bird.x;
      this.emitter.y = bird.y;
      this.emitter.start(true, 1000, null, 30);
      
      // CAMERA EFFECT
      //game.camera.flash(0xffffff, 300);
      game.camera.shake(0.02, 1000);
  
      game.time.events.add(2000, function() {
        game.state.start('menu');
      }, this);
    }
  };