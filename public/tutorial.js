var tutorialState = {
    create: function() {
      game.add.image(0, 0, 'tutorialbg');
    },
    update: function() {
      var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
      upKey.onDown.add(this.start1, this);
      downKey.onDown.add(this.start2, this);
    },
    start1: function() {
      // Start the actual game
      game.global.state = 1;
      game.state.start('play');
    },
    start2: function() {
      // Start the actual game
      game.global.state = 2;
      game.state.start('play');
    },
  };
  game.state.add('tutorial', tutorialState);