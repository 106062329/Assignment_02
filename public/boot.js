var bootState = {
    preload: function () {
      // Load the progress bar image.
      game.load.image('progressBar', 'assets/progressBar.png');
      game.load.image('menubg', 'assets/menubg.jpg');
    },
    create: function() {
      // Set some game settings.
      game.add.image(0, 0, 'menubg');
      game.physics.startSystem(Phaser.Physics.ARCADE);
      game.renderer.renderSession.roundPixels = true;
      // Start the load state.
      game.state.start('load');
    }
  }; 