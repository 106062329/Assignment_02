var loadState = {
    ready: false,
    preload: function () {
      game.add.image(0, 0, 'menubg');
      // Add a 'loading...' label on the screen
      var loadingLabel = game.add.text(game.width/2, 150,
      'loading...', { font: '30px Arial', fill: '#ffffff' });
      loadingLabel.anchor.setTo(0.5, 0.5);
      // Display the progress bar
      var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
      progressBar.anchor.setTo(0.5, 0.5);
      game.load.setPreloadSprite(progressBar);
      // Load all game assets
      game.load.spritesheet('player', 'assets/player.png', 50, 29);
      game.load.spritesheet('player2', 'assets/player2.png', 50, 29);
      game.load.spritesheet('bird', 'assets/birds.png',40,40);
      game.load.spritesheet('bird_y', 'assets/birds_yellow.png', 40, 40);
      game.load.spritesheet('bird_r', 'assets/birds_red.png', 40, 40);
      game.load.spritesheet('boss', 'assets/big.png', 80, 78);
      game.load.image('bullet', 'assets/seed_small.png');
      game.load.image('seed_y', 'assets/seed_red.png');
      game.load.image('seed_r', 'assets/seed_yellow.png');
      game.load.image('tutorialbg', 'assets/tutorial.jpg');
      //game.load.image('coin', 'assets/coin.png');
      game.load.image('heart', 'assets/heart.png');
      game.load.image('three','assets/3.png');
      game.load.image('wallV', 'assets/wallVertical.png');
      game.load.image('wallH', 'assets/wallHorizontal.png');
      // Load a new asset that we will use in the menu state
      game.load.image('bgtile', 'assets/bg.jpg');
  
      game.load.image('pixel', 'assets/pixel.png');
      game.load.audio('bgm', 'assets/bgm.mp3');
      game.load.audio('shoot', 'assets/new_shoot.mp3');
    },
    update: function() {
        if(this.ready){
            return;
        }
        if(game.cache.isSoundDecoded('shoot') && game.cache.isSoundDecoded('bgm')){
            this.ready = true;
            game.state.start('menu');
        }
    },
    /*create: function() {
      // Go to the menu state
      game.state.start('menu');
    }*/
  }; 