var menuState = {
    create: function() {
      // Add a background image
      game.add.image(0, 0, 'menubg');
      // Display the name of the game
      // PUSH SCORE
      if(game.global.score!=0){
        var Ref = firebase.database().ref("score_list");
        if(game.global.name == '')game.global.name = 'unknown';
        var text={
          name: game.global.name,
          score: game.global.score
        }
        var pushRef = Ref.push(text);
      }

      this.scoreboard = [];
      this.nameboard = [];
      this.getscores();

      this.nowdown;
      game.global.name = "";
      
      // Show the score at the center of the screen
      this.scoreLabel1 = game.add.text(game.width/2, game.height/2-30,
      '1. ', { font: '20px Arial', fill: '#c596f4' });
      this.nameLabel1 = game.add.text(game.width/2+200, game.height/2-30,
      '0', { font: '20px Arial', fill: '#c596f4' });
      this.scoreLabel2 = game.add.text(game.width/2, game.height/2,
      '2. ', { font: '20px Arial', fill: '#c596f4' });
      this.nameLabel2 = game.add.text(game.width/2+200, game.height/2,
      '0', { font: '20px Arial', fill: '#c596f4' });
      this.scoreLabel3 = game.add.text(game.width/2, game.height/2+30,
      '3. ', { font: '20px Arial', fill: '#c596f4' });
      this.nameLabel3 = game.add.text(game.width/2+200, game.height/2+30,
      '0', { font: '20px Arial', fill: '#c596f4' });
      this.scoreLabel = game.add.text(game.width/2, game.height/2+60,
      'your score: ', { font: '20px Arial', fill: '#c596f4' });
      this.myscoreLabel = game.add.text(game.width/2+200, game.height/2+60,
      game.global.score, { font: '20px Arial', fill: '#c596f4' });
      

      this.playernameLabel = game.add.text(game.width/2, game.height/2+90,
      'your name: ' + game.global.name, { font: '20px Arial', fill: '#c596f4' });
      // this.instruct = game.add.text(game.width/2, game.height/2+105,
      //   'press space between entering same letters' + game.global.name, { font: '10px Arial', fill: '#c596f4' });

      
      // var nameLabel = game.add.text(game.width/2, -50, 'Raiden', { font: '50px Arial', fill: '#ffffff' });
      // nameLabel.anchor.setTo(0.5, 0.5);
      // game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
      
      // Explain how to start the game
      // var startLabel = game.add.text(game.width/2, game.height-30, 'press the up arrow key to start', { font: '25px Arial', fill: '#c596f4' });
      // startLabel.anchor.setTo(0.5, 0.5);
      // game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    },
    update: function() {
      if(this.scoreboard[this.scoreboard.length-1]){
        this.scoreLabel1.text = '1. ' + this.nameboard[this.nameboard.length-1];
        this.scoreLabel2.text = '2. ' + this.nameboard[this.nameboard.length-2];
        this.scoreLabel3.text = '3. ' + this.nameboard[this.nameboard.length-3];
        this.nameLabel1.text = this.scoreboard[this.scoreboard.length-1];
        this.nameLabel2.text = this.scoreboard[this.scoreboard.length-2];
        this.nameLabel3.text = this.scoreboard[this.scoreboard.length-3];
      }else{
        this.scoreLabel1.text = '1. ';
        this.scoreLabel2.text = '2. ';
        this.scoreLabel3.text = '3. ';
      }
      // Create a new Phaser keyboard variable: the up arrow key
      // When pressed, call the 'start'
      var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
      var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
      upKey.onDown.add(this.start1, this);
      downKey.onDown.add(this.start2, this);
      if(game.input.keyboard.isDown(Phaser.Keyboard.A) && this.nowdown != Phaser.Keyboard.A) {
        this.nowdown = Phaser.Keyboard.A;
        game.global.name = game.global.name + 'A';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.B) && this.nowdown != Phaser.Keyboard.B) {
        this.nowdown = Phaser.Keyboard.B;
        game.global.name = game.global.name + 'B';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.C) && this.nowdown != Phaser.Keyboard.C) {
        this.nowdown = Phaser.Keyboard.C;
        game.global.name = game.global.name + 'C';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.D) && this.nowdown != Phaser.Keyboard.D) {
        this.nowdown = Phaser.Keyboard.D;
        game.global.name = game.global.name + 'D';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.E) && this.nowdown != Phaser.Keyboard.E) {
        this.nowdown = Phaser.Keyboard.E;
        game.global.name = game.global.name + 'E';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.F) && this.nowdown != Phaser.Keyboard.F) {
        this.nowdown = Phaser.Keyboard.F;
        game.global.name = game.global.name + 'F';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.G) && this.nowdown != Phaser.Keyboard.G) {
        this.nowdown = Phaser.Keyboard.G;
        game.global.name = game.global.name + 'G';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.H) && this.nowdown != Phaser.Keyboard.H) {
        this.nowdown = Phaser.Keyboard.H;
        game.global.name = game.global.name + 'H';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.I) && this.nowdown != Phaser.Keyboard.I) {
        this.nowdown = Phaser.Keyboard.I;
        game.global.name = game.global.name + 'I';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.J) && this.nowdown != Phaser.Keyboard.J) {
        this.nowdown = Phaser.Keyboard.J;
        game.global.name = game.global.name + 'J';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.K) && this.nowdown != Phaser.Keyboard.K) {
        this.nowdown = Phaser.Keyboard.K;
        game.global.name = game.global.name + 'K';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.L) && this.nowdown != Phaser.Keyboard.L) {
        this.nowdown = Phaser.Keyboard.L;
        game.global.name = game.global.name + 'L';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.M) && this.nowdown != Phaser.Keyboard.M) {
        this.nowdown = Phaser.Keyboard.M;
        game.global.name = game.global.name + 'M';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.N) && this.nowdown != Phaser.Keyboard.N) {
        this.nowdown = Phaser.Keyboard.N;
        game.global.name = game.global.name + 'N';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.O) && this.nowdown != Phaser.Keyboard.O) {
        this.nowdown = Phaser.Keyboard.O;
        game.global.name = game.global.name + 'O';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.P) && this.nowdown != Phaser.Keyboard.P) {
        this.nowdown = Phaser.Keyboard.P;
        game.global.name = game.global.name + 'P';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.Q) && this.nowdown != Phaser.Keyboard.Q) {
        this.nowdown = Phaser.Keyboard.Q;
        game.global.name = game.global.name + 'Q';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.R) && this.nowdown != Phaser.Keyboard.R) {
        this.nowdown = Phaser.Keyboard.R;
        game.global.name = game.global.name + 'R';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.S) && this.nowdown != Phaser.Keyboard.S) {
        this.nowdown = Phaser.Keyboard.S;
        game.global.name = game.global.name + 'S';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.T) && this.nowdown != Phaser.Keyboard.T) {
        this.nowdown = Phaser.Keyboard.T;
        game.global.name = game.global.name + 'T';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.U) && this.nowdown != Phaser.Keyboard.U) {
        this.nowdown = Phaser.Keyboard.U;
        game.global.name = game.global.name + 'U';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.V) && this.nowdown != Phaser.Keyboard.V) {
        this.nowdown = Phaser.Keyboard.V;
        game.global.name = game.global.name + 'V';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.W) && this.nowdown != Phaser.Keyboard.W) {
        this.nowdown = Phaser.Keyboard.W;
        game.global.name = game.global.name + 'W';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.X) && this.nowdown != Phaser.Keyboard.X) {
        this.nowdown = Phaser.Keyboard.X;
        game.global.name = game.global.name + 'X';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.Y) && this.nowdown != Phaser.Keyboard.Y) {
        this.nowdown = Phaser.Keyboard.Y;
        game.global.name = game.global.name + 'Y';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.Z) && this.nowdown != Phaser.Keyboard.Z) {
        this.nowdown = Phaser.Keyboard.Z;
        game.global.name = game.global.name + 'Z';
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.nowdown != Phaser.Keyboard.SPACEBAR) {
        this.nowdown = Phaser.Keyboard.SPACEBAR;
        game.global.name = game.global.name;
        console.log(game.global.name);
      }else if(game.input.keyboard.isDown(Phaser.Keyboard.BACKSPACE) && this.nowdown != Phaser.Keyboard.BACKSPACE) {
        this.nowdown = Phaser.Keyboard.BACKSPACE;
        game.global.name = game.global.name.substring(0,game.global.name.length-1);
        console.log(game.global.name);
      }
      this.playernameLabel.text = 'your name: ' + game.global.name;
    },
    getscores:function() {
      var board = [];
      var n_board = [];
      var databaseRef = firebase.database().ref('score_list');
      databaseRef.orderByChild("score").once('value', function (snapshot) {
          snapshot.forEach(function (item) {
              var score = item.val().score;
              var name = item.val().name;
              n_board.push(name);
              board.push(score);
          })
      })
      this.scoreboard = board;
      this.nameboard = n_board;
    },
    start1: function() {
      // Start the actual game
      game.global.state = 1;
      game.state.start('tutorial');
    },
    start2: function() {
      // Start the actual game
      game.global.state = 2;
      game.state.start('tutorial');
    },
  }; 