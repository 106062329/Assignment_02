# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-player game(Off-line)|5%|Y|
|Enhanced items * 2||Y|
|Boss|5%|Y|
|Tutorial page||Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
    - 玩家利用上下左右(player2 : WASD)移動
    - O(G)發射一般子彈
    - P(H)消耗30點技能點發射特殊子彈。
    - 分數會隨存活時間增長或擊中敵人而增加，隨分數上升，出現的敵人種類逐漸增加。
    - 上升到最後階段會出現BOSS，打敗BOSS或玩家血量歸零結束遊戲。
2. Animations : 
    - 玩家上下移動時紙飛機有不同角度
    - 敵方鳥類行進時會拍翅。
3. Particle Systems : 以下狀況都會產生粒子效果
    - 玩家子彈射到敵方鳥類
    - 敵方子彈射到玩家
    - 玩家超出遊戲範圍
4. Sound effects : 
    - 遊戲進行時會有背景音樂
    - 玩家發射子彈時有發射音效
    - 遊戲進行時長按I降低音量，U提高音量。
5. Leaderboard : 
    - 遊戲開始前可以輸入名稱(輸入時若要連續輸入兩次相同字母，要在中間輸入一次空白鍵)
    - 遊戲結束後可以看到自己的分數以及歷來成績的前三名。
6. Pause : 
    - 遊戲畫面左下角有個Pause鍵，點擊即可暫停遊戲
    - 再度點擊畫面任一處可以繼續遊戲。

# Bonus Functions Description : 
1. Multi-player game(Off-line) :
    - 在menu畫面按上進行單人模式，按下進入雙人模式
    - 操作方式在上面的Jucify mechanisms
    - 玩家二的相關資訊在右下角
    - 當其中一個玩家死亡，另一個玩家可以繼續遊戲直到雙方都死亡或打敗boss
2. Enhanced items : 
    - 玩家吃到隨機出現的愛心可以補充血量。
    - 玩家吃到隨機出現的x3標示，接下來10發基本子彈變成一排三個。
3. Boss : 
    - 遊戲進行到最後會有一隻紅色大鳥
    - 移動方式隨機上下左右，一次發射三發散射子彈
    - 打敗即結束遊戲
4. Different enemies : 
    - 白鳥 : 直線移動，發射直線子彈
    - 黃鳥 : 隨機變換Y軸速度，發射直線子彈
    - 紅鳥 : 直線移動，發射射向player的子彈
5. Tutorial page :
    從menu跳轉到遊戲畫面前會有操作教學的畫面。

